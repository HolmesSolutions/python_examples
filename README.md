# Python Examples
*A simple Python package to help people learn Python 3.5+*

Latest version at:
https://gitlab.com/HolmesSolutions/python_examples

## About
This repository is structured as a package to help people learn Python. Many of the modules (*.py files) have a `demonstrate` method, which tries to illustrate the main lesson of the module.

Some examples assume the use of the Spyder IDE.
